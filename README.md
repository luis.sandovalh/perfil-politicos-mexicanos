# Perfil De Políticos Mexicanos
Este es un proyecto hecho con [Scrapy](https://docs.scrapy.org/en/latest/index.html) que obtiene la información de los políticos mexicanos del [SIL](http://sil.gobernacion.gob.mx/portal) y los exporta en formato JSON utilizando el estándar de [Popolo](https://www.popoloproject.com/) para datos abiertos.


## Exportar diputados

Puedes obtener los diputados de la LXV legislatura en el archivo `federal_politicians/exports/deputies.json` o corre el siguiente comando:

`$ scrapy crawl deputies -O deputies.json`

## Exportar senadores

Puedes obtener los senadores de la LXV legislatura en el archivo `federal_politicians/exports/senators.json` o corre el siguiente comando:

`$ scrapy crawl senators -O senators.json`

## Exportar diputados y senadores de otras legislaturas

Puedes exportar los diputados y senadores de otras legislaturas visitando el [formulario de búsqueda](http://www.sil.gobernacion.gob.mx/portal/Legislador/busquedaInstancia)  y corre este comando en la consola del navegador para obtener los ids de los políticos.

```js
JSON.stringify(Array.from(document.querySelectorAll("#main > table:nth-child(7) tr > td a[onclick]")).map(el => {
 if (el.attributes.onclick) { return `http://www.sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=${el.getAttribute("onclick").match(/(Referencia=)(\d*)/g)[0].substring(11)}`}}))
```

Pega los URLs en la variable `start_urls` del archivo `deputies_spider.py` o `senators_spider.py`.
## Avances
- [x] Exportar diputados
- [x] Exportar senadores
