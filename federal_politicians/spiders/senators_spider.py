import scrapy
import re
from unidecode import unidecode
import stringcase


class SenatorsSpider(scrapy.Spider):
    name = "senators"
    start_urls = [
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225013",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225251",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225207",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224913",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225093",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225037",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225159",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225173",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225309",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225259",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9226979",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225279",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225073",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9226971",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224931",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225057",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224966",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225196",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225091",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225117",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225149",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224933",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225663",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9227184",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225186",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9227149",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225122",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224897",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225175",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225115",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224935",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225285",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225190",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224925",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225120",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225197",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9226975",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224953",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224974",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225127",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224915",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224977",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224981",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224941",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9227171",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9226973",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225304",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9226968",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225133",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225125",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225154",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225097",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225169",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224985",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225177",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225171",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224959",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225290",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225103",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225233",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225217",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224909",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225227",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224893",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224895",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225307",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225025",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225155",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224969",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225302",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225041",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9226969",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9226977",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225283",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225161",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225137",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225300",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9227135",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225306",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225236",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225219",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224961",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225102",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224949",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224958",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225255",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225129",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225257",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224993",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225183",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225200",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225212",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225164",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9227183",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225165",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225113",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225089",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224989",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224929",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225205",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224968",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225311",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225229",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225249",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9227160",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225141",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225666",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225111",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225210",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225124",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225298",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225193",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225107",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225135",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225131",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225297",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225296",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225225",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9226993",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225106",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225201",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9224902",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9227159",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225069",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225321",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9227182",
        "http://sil.gobernacion.gob.mx/Librerias/pp_PerfilLegislador.php?SID=&Referencia=9225231"
    ]

    def parse(self, response):
        parsed_response = response.replace(body=re.sub(r'(<\/tr>\s*<table)', '</tr></table><table',response.css('html').get()))
        pName = ''
        pEmail = ''
        pActive = False
        pParty = ''
        pBirthDate = ''
        pTelephone = ''
        pContactDetails = []
        pLegislativeExperience = None
        pSwearingDate = ''
        pAcademicPreparation = None
        pLocation = ''
        pLastDegreeStudies = None
        pLegislature = ''
        pState = ''
        pDistrict = ''
        pCity = ''
        pRelativemayority = False
        pPhotoUrls = []

        # Politician Prfile
        self.log(f'\n\n')
        self.log(f'Scraping Politician Profile')

        pPhotoUrls.append('http://www.sil.gobernacion.gob.mx' + parsed_response.css("#Nombredelancla img").attrib["src"])

        for data in parsed_response.css('table:nth-child(5) table tr'):
            property = ' '.join(data.css('.tdcriterioPer *::text').extract())

            if property is not None:
                property = stringcase.snakecase(re.sub("\s\s+" , " ", unidecode(property.strip().replace(u':',u'').replace(u'  ',u' ')))).lower()
            
            value = data.css('.tddatosazulPer')

            if property == 'nombre':
                pName = value.css('b::text').get()
                pLegislature = re.search('(por la )([I|V|X|L|C|D|M]*)( Legislatura)', value.css("::text").extract().pop()).group(2)
            elif property == 'partido':
                pParty = value.css('::text').get()
            elif property == 'nacimiento':
                pBirthDate = re.search(r'(\d+/\d+/\d+)', value.css('::text').get()).group(1)
            elif property == 'estatus':
                pActive = value.css('::text').get() != 'Baja'
            elif property == 'correo_electronico':
                pEmail = value.css('::text').get()
            elif property == 'telefono':
                pTelephone = value.css('::text').get()
                pContactDetails.append({
                    'type': 'telephone',
                    'label': 'Telephone',
                    'value': pTelephone
                })
            elif property == 'experiencia_legislativa':
                pLegislativeExperience = {
			        'es_MX': value.css('::text').extract().pop()
                }
            elif property == 'toma_de_protesta':
                pSwearingDate = value.css('::text').get()
            elif property == 'preparacion_academica':
                pAcademicPreparation = {
			        'es_MX': value.css('::text').get()
                }
            elif property == 'ubicacion':
                pLocation = value.css('::text').get()
            elif property == 'ultimo_grado_de_estudios':
                pLastDegreeStudies = {
			        'es_MX': value.css('::text').get()
                }
            elif property == 'principio_de_eleccion':
                pRelativemayority = value.css('::text').get() == 'Mayoría Relativa'
            elif property == 'zona':
                zoneValues = value.css("*::text").extract()
                self.log(f'zoneValues {zoneValues}')
                for zone in zoneValues:
                    self.log(f'zone {zone}')
                    entidad = re.search("(Entidad: )(.*)$", zone.strip())
                    if entidad is not None:
                        pState = entidad.group(2)

                    city = re.search("\((.*)\)$", zone.strip())
                    if city is not None:
                        pCity = city.group(1)

                    district = re.search("(\d+)$", zone.strip())
                    if district is not None:
                        pDistrict = district.group(0)

        # Legislative History
        self.log(f'\n\n')
        self.log(f'Legislative History')

        pCommitteeHistory = []
        pAdministrativeHistory = []
        pLegislativeHistory = []
        pPoliticalHistory = []
        pAcademicHistory = []
        pOtherExperience = []
        pPrivateExperience = []

        tables = parsed_response.css('body > table')
        for index, data in enumerate(tables):
            tableName = data.css("td *::text").get()
            self.log(f'tableName: {tableName}')
            if tableName is not None and ('TRAYECTORIA' in tableName or "OTROS" in tableName or "COMISIONES" in tableName):
                for j, dataj in enumerate(tables[index + 1].css("table tr")):                   
                    if j > 0:
                        row = dataj.css('td')
                        item = {
                            'start_year': row[0].css("::text").get(),
                            'end_year': row[1].css("::text").get(),
                            'description': {
                                'es_MX': row[2].css("::text").get()
                            }
                        }
                        # self.log(f'item: {item}')
                        if tableName == 'COMISIONES':
                            pCommitteeHistory.append({
                                'name': {
                                    'es_MX': row[0].css("::text").get(),
                                },
                                'role': {
                                    'es_MX': row[1].css("::text").get(),
                                },
                                'start_date': row[2].css("::text").get(),
                                'end_date': row[3].css("::text").get(),
                                'active': row[4].css("::text").get() != 'Baja'
                            })
                        elif tableName == 'TRAYECTORIA ADMINISTRATIVA':
                            pAdministrativeHistory.append(item)
                        elif tableName == 'TRAYECTORIA LEGISLATIVA':
                            pLegislativeHistory.append(item)
                        elif tableName == 'TRAYECTORIA POLÍTICA':
                            pPoliticalHistory.append(item)
                        elif tableName == 'TRAYECTORIA ACADÉMICA':
                            pAcademicHistory.append(item)
                        elif tableName == 'TRAYECTORIA EMPRESARIAL/INICIATIVA PRIVADA':
                            pPrivateExperience.append(item)
                        elif tableName == 'OTROS RUBROS':
                            pOtherExperience.append(item)

        yield {
            'name': pName,
            'email': pEmail,
            'legislature': pLegislature,
            'active': pActive,
            'party': pParty,
            'birth_date': pBirthDate,
            'telephone': pTelephone,
            'contact_details': pContactDetails,
            'photo_urls': pPhotoUrls,
            'legislative_experience': pLegislativeExperience,
            'swearing_date': pSwearingDate,
            'academic_preparation': pAcademicPreparation,
            'location': pLocation,
            'last_degree_of_studies': pLastDegreeStudies,
            'state': pState,
            'district': pDistrict,
            'city': pCity,
            'relative_majority': pRelativemayority,
            'committee_history': pCommitteeHistory,
            'administrative_history': pAdministrativeHistory,
            'legislative_history': pLegislativeHistory,            
            'political_history': pPoliticalHistory,
            'academic_history': pAcademicHistory,
            'private_experience': pPrivateExperience,
            'other_experience': pOtherExperience            
        }

        self.log(f'\n\n')
